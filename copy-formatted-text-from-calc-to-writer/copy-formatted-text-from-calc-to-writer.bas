'************************************************************************************
'**
'** Output the formatted text of a Cell in a LibreOffice Calc document to 
'** a Libreoffice Writer document and keep the following formatting styles
'** of the text:
'** 
'** - Bold 
'** - Underline
'** - Italics
'** - Superscript/subscript 
'** - Text color
'** 
'** 12. June 2017 Martin Sauter
'**
'************************************************************************************

Option Explicit

Const my_DefaultFontName = "Times New Roman"

Sub Main

  'Variables to loop over the calc cells
  DIM CalcDoc
  Dim Sheet
  DIM iColumn as Long
  DIM iLine as Long
  DIM my_Cell

  Dim Url
  Dim Doc
  Dim Dummy()
  Dim TextCursor
     
  ' Get a reference the the first sheet of the current calc document
  CalcDoc = ThisComponent
  Sheet = CalcDoc.Sheets(0)   
    
  'Open new writer document
  Url = "private:factory/swriter"
  Doc = StarDesktop.loadComponentFromURL(Url, "_blank", 0, Dummy())
  
  'Get a text cursor for inserting stuff at the end of the document
  TextCursor = Doc.getText.getEnd()

  ' ######################################################################        
 
  'Output content of the cell
  iColumn = 0
  iLine = 1

  my_Cell = Sheet.getCellByPosition(iColumn,iLine)

  my_copyCellContentToWriterWithFormatting(my_Cell, Doc)
  
  ' ######################################################################        
  
End Sub


'****************************************************************
'** Copy the text in a Calc cell to the given Writer document
'** and keep the formatting
'** 
'** Note: At the moment only some but NOT all formatting
'**       options are treated, add as required.
'**
'** See for example: 
'**   https://ask.libreoffice.org/en/question/
'**   12493/how-can-i-use-a-macro-in-basic-to-copy-formatted-text-
'**   data-from-spreadsheet-to-a-text-document/
'**
'****************************************************************

Function my_copyCellContentToWriterWithFormatting (CurCell, OutputDocument)

  Dim CurLine As String

  Dim TextElement As Object, TextPortion As Object
  Dim TempString

  Dim OutputDocCursor  'And an output document cursor
  OutputDocCursor = OutputDocument.getText.getEnd()

  DIM NewParagraph
  NewParagraph = com.sun.star.text.ControlCharacter.APPEND_PARAGRAPH

  Dim Enum1 As Object, Enum2 As Object
  Enum1 = CurCell.Text.createEnumeration
   
    
  ' loop over all paragraphs
  While Enum1.hasMoreElements
    TextElement = Enum1.nextElement
   
    If TextElement.supportsService("com.sun.star.text.Paragraph") Then
      Enum2 = TextElement.createEnumeration
   
      ' loop over all paragraph portions
      While Enum2.hasMoreElements
        TextPortion = Enum2.nextElement

        'if formatting changed, output all previous characters with the last formatting
        OutputDocCursor.CharPosture = TextPortion.CharPosture     
        OutputDocCursor.CharWeight = TextPortion.CharWeight
        OutputDocCursor.CharColor = TextPortion.CharColor
        OutputDocCursor.CharUnderline = TextPortion.CharUnderline

        'Superscript/Subscript text        
        OutputDocCursor.CharEscapement = TextPortion.CharEscapement
        OutputDocCursor.CharEscapementHeight = TextPortion.CharEscapementHeight

        outputDocument.getText.insertString(OutputDocCursor, TextPortion.GetString(), False)
         
      Wend

      OutputDocument.getText.InsertControlCharacter(OutputDocCursor, NewParagraph, False)

    End If

  Wend
  
End Function

